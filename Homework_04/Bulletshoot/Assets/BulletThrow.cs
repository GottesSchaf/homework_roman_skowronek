﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletThrow : MonoBehaviour {

    public float speed, grav, winkel, rotateSpeed = 1f;
    float waiter = 150;
    float timer;
    bool canRotate = true;
    Vector3 myPos;
    public GameObject orbitCenter;
    public float distance;
    // Use this for initialization
    void Start () {
        //set coordinations to a fixed point
        myPos = orbitCenter.transform.position;
        myPos.x = distance * Mathf.Sin(rotateSpeed);
        myPos.y = distance * Mathf.Cos(rotateSpeed);
        this.transform.position = myPos;
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKey(KeyCode.DownArrow) && canRotate)
        {
            OrbitAroundDown();
        }
        else if (Input.GetKey(KeyCode.UpArrow) && canRotate)
        {
            OrbitAroundUp();
        }

        if (Input.GetKey(KeyCode.Space))
        {
            timer = Time.time;
            StartCoroutine(Shooter());
        }
	}

    void OrbitAroundDown()
    {
        rotateSpeed += 0.02f;
        if (orbitCenter != null)
        {
            Vector3 myPos = orbitCenter.transform.position;
            myPos.x += distance * Mathf.Sin(rotateSpeed);
            myPos.y += distance * Mathf.Cos(rotateSpeed);

            this.transform.position = myPos;
        }
        //if the radian is bigger then 1.5 hold it at 1.5
        if (rotateSpeed >= 1.5f)
        {
            rotateSpeed = 1.5f;
        }
    }

    void OrbitAroundUp()
    {
        rotateSpeed -= 0.02f;
        if (orbitCenter != null)
        {
            Vector3 myPos = orbitCenter.transform.position;
            myPos.x += distance * Mathf.Sin(rotateSpeed);
            myPos.y += distance * Mathf.Cos(rotateSpeed);

            this.transform.position = myPos;
        }
        //if the radian is smaller then 0.5 hold it at 0.5
        if (rotateSpeed <= 0.5f)
        {
            rotateSpeed = 0.5f;
        }
    }

    IEnumerator Shooter()
    {
        canRotate = false; //turns off the ability to rotate the cube with the arrow keys...
        //for a period of time calculate the fall
        for (int i = 0; i < waiter; i++)
        {
            myPos.x = distance + speed * (Time.time - timer) * Mathf.Sin(rotateSpeed);
            myPos.y = distance + speed * (Time.time - timer) * Mathf.Cos(rotateSpeed) - (grav / 2) * Mathf.Pow(Time.time - timer, 3);
            this.transform.position = myPos;
            yield return new WaitForSeconds(0.00001f);
        }
        //after that bring it back to the starting point
        myPos.x = distance * Mathf.Sin(rotateSpeed);
        myPos.y = distance * Mathf.Cos(rotateSpeed);
        this.transform.position = myPos;
        canRotate = true; //...and turns it back on
        StopCoroutine(Shooter());
    }

    private void OnTriggerEnter(Collider col)
    {
        if (col.CompareTag("Respawn"))
        {
            Vector3 myPos = orbitCenter.transform.position;
            myPos.x = distance * Mathf.Sin(rotateSpeed);
            myPos.y = distance * Mathf.Cos(rotateSpeed);
            this.transform.position = myPos;
            StopCoroutine(Shooter());
        }
    }
}
