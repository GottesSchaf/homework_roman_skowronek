﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomMazeGenerator : MonoBehaviour {


	[SerializeField] GameObject m_mazeElement;
    public float m_distanceMazeBlocks;

	[SerializeField] int m_mazeWidth;
	[SerializeField] int m_mazeHeight;

    bool[,] m_mazeGrid;
    List<Vector2> m_directions;

	/// <summary>
	/// Generate a Maze on Startup
	/// </summary>
	void Start () {
        m_mazeGrid = new bool[m_mazeWidth, m_mazeHeight];

        m_directions = new List<Vector2> { Vector2.up, Vector2.down, Vector2.left, Vector2.right };

		GenerateMaze ();
	}
		
	/// <summary>
	/// Place randomly the two different maze elements
	/// </summary>
	void GenerateMaze()
	{
        CarvePassageFrom(1, 1);
        CreateMazeBlocks();
    }

    void CarvePassageFrom(int _xParameter, int _yParameter)
    {
        foreach(Vector2 direction in m_directions)
        {
            int new_x = _xParameter + (int)direction.x * 2;
            int new_y = _yParameter + (int)direction.y * 2;

            int inbetween_x = _xParameter + (int)direction.x;
            int inbetween_y = _yParameter + (int)direction.y;

            if (new_x >= 0 && new_x < m_mazeWidth && new_y >= 0 && new_y < m_mazeHeight && m_mazeGrid[new_x, new_y] != true)
            {
                m_mazeGrid[_xParameter, _yParameter] = true;
                m_mazeGrid[inbetween_x, inbetween_y] = true;
                m_mazeGrid[new_x, new_y] = true;

                CarvePassageFrom(new_x, new_y);
            }
        }
    }

    void CreateMazeBlocks()
    {
        Vector3 elementPosition = Vector3.zero;
        GameObject elementObject = null;
        int counter_width = 0;
        int counter_height = 0;

        /// itterate through two loops to the height and width of the maze

        while (counter_width < m_mazeWidth)
        {
            while (counter_height < m_mazeHeight)
            {
                if (m_mazeGrid[counter_width, counter_height] == false)
                {
                    ///calculate the position of the element from the center
                    elementPosition.x = (counter_width - (m_mazeWidth / 2)) * m_distanceMazeBlocks;
                    elementPosition.z = (counter_height - (m_mazeHeight / 2)) * m_distanceMazeBlocks;

                    ///instantiate the element
                    elementObject = Instantiate(m_mazeElement, elementPosition, m_mazeElement.transform.rotation);
                    elementObject.name = "MazeElement_";
                    elementObject.name += counter_width + "_" + counter_height;
                }

                counter_height++;
            }

            counter_height = 0;
            counter_width++;

        }
    }
}
