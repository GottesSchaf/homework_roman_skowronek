﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomMazeGenerator : MonoBehaviour {


	[SerializeField] GameObject[] m_mazeElement; //Abgeändert in ein Array

	[SerializeField] int m_mazeWidth;
	[SerializeField] int m_mazeHeight;
    int ran;

	/// <summary>
	/// The distance between the maze elements.
	/// </summary>
	[SerializeField] float m_distanceMazeElements;

	/// <summary>
	/// Generate a Maze on Startup
	/// </summary>
	void Start () {

		GenerateMaze ();
	}
		
	/// <summary>
	/// Place randomly the two different maze elements
	/// </summary>
	void GenerateMaze()
	{
		Vector3 elementPosition = Vector3.zero  ;
		GameObject elementObject = null;
		int counter_width = 0; 
		int counter_height = 0; 

		///itterate through two loops to the height and width of the maze 
		while(counter_width < m_mazeWidth) 
		{
			while (counter_height < m_mazeHeight) 
			{
				///calculate the position of the element from the center
				elementPosition.x = (counter_width - (m_mazeWidth / 2)) * m_distanceMazeElements;
				elementPosition.z = (counter_height - (m_mazeHeight / 2)) * m_distanceMazeElements;

                //--------------------------------------------------------------------------neuer Code--------------------------------------------------------------------------------
                ran = Random.Range(0, 5); //Erzeuge eine Random Nummer zwischen 0 und 5
                elementObject = Instantiate(m_mazeElement[ran], elementPosition, m_mazeElement[ran].transform.rotation); //Erstelle das Objekt mit der Random Nummer zwischen 0 und 5
                //------------------------------------------------------------------------ende neuer Code-----------------------------------------------------------------------------

                elementObject.name += counter_width + "_" + counter_height;

				counter_height++;
			}

			counter_height = 0;
			counter_width++;

		}
	}
}
