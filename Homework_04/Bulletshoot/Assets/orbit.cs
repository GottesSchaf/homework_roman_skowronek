﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class orbit : MonoBehaviour {

    public GameObject orbitCenter;
    public float distance;
	
	// Update is called once per frame
	void Update () {
		if(orbitCenter != null) //&& (Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.W))
        {
            Vector3 myPos = orbitCenter.transform.position;
            myPos.x += distance * Mathf.Cos(Time.time);
            myPos.y += distance * Mathf.Sin(Time.time);

            this.transform.position = myPos;
        }
        //else if (orbitCenter != null && (Input.GetKeyDown(KeyCode.DownArrow) || Input.GetKeyDown(KeyCode.S)))
        //{
        //    Vector3 myPos = orbitCenter.transform.position;
        //    myPos.x += distance * Mathf.Tan(Time.time);
        //    myPos.y += distance * Mathf.Cos(Time.time);
        //}
        //else
        //{
        //    Debug.Log("No Orbit Center attached!");
        //}
	}
}
