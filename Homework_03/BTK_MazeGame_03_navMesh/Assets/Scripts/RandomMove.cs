﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class RandomMove : MonoBehaviour {

	NavMeshAgent myAgent;

	void Start () {
		myAgent = GetComponent<NavMeshAgent> ();
		MoveToRandomPosition ();
	}
	
	// Update is called once per frame
	public void MoveToRandomPosition () {
		Vector3 randomPos = new Vector3 (Random.Range (-10f, 10f), 0.5f, Random.Range (-10f, 10f));
		myAgent.destination = randomPos;

		Debug.Log ("New Path calculation to " + randomPos);
	}

	void Update (){
		if (!myAgent.hasPath || gameObject.transform.position == myAgent.destination) {
			MoveToRandomPosition ();
		}
	}

	void OnCollisionEnter (Collision col){
		if (col.gameObject.GetComponent<RandomMove> ()) {
			Destroy (col.gameObject);
		}
	}
}
